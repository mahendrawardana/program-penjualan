<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBarang extends Model
{
    use SoftDeletes;

    protected $table = 'barang';
    protected $primaryKey = 'id_barang';
    protected $fillable = [
        'id_jenis_barang',
        'brg_kode',
        'brg_nama',
        'brg_golongan',
        'brg_minimal_stok',
        'brg_keterangan'
    ];

    public function jenis_barang()
    {
        return $this->belongsTo(mJenisBarang::class, 'id_jenis_barang');
    }

    public function satuan()
    {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }

    public function stok_barang()
    {
        return $this->hasMany(mStokBarang::class, 'id_barang');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
