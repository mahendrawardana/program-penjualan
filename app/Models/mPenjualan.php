<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPenjualan extends Model
{
    use SoftDeletes;

    protected $table = 'penjualan';
    protected $primaryKey = 'id_penjualan';
    protected $fillable = [
        'id_pelanggan',
        'id_user',
        'pjl_urutan',
        'pjl_no_faktur',
        'pjl_tanggal_penjualan',
        'pjl_jenis_pembayaran',
        'pjl_total',
        'pjl_biaya_tambahan',
        'pjl_potongan',
        'pjl_grand_total',
        'pjl_jumlah_bayar',
        'pjl_sisa_pembayaran',
        'pjl_keterangan',
        'pjl_jatuh_tempo',
        'pjl_midtrans_payment_link',
        'pjl_status',
        'pjl_ongkos_kirim',
        'pjl_jenis_kurir',
    ];

    public function pembeli()
    {
        return $this->belongsTo(mPembeli::class, 'id_penjualan', 'id_penjualan');
    }

    public function pelanggan()
    {
        return $this->belongsTo(mPelanggan::class, 'id_pelanggan');
    }

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function penjualan_detail()
    {
        return $this->hasMany(mPenjualanDetail::class, 'id_penjualan');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
