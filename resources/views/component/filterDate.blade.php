<div class="m-portlet m-portlet--tab">
    <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row text-center">
                <label class="col-form-label col-lg-2 col-sm-12">Rentang Tanggal</label>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <input type='text'
                           class="form-control m_datepicker_1_modal"
                           readonly
                           value="{{ $date_from }}"
                           placeholder="Select time"
                           name="date_from"/>
                </div>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <input type='text'
                           class="form-control m_datepicker_1_modal"
                           readonly
                           value="{{ $date_to }}"
                           placeholder="Select time"
                           name="date_to"/>
                </div>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <select class="form-control">
                        
                    </select>
                </div>
                <div class="col-lg-1 col-sm-12">
                    <button type="submit" class="btn btn-accent m-btn--pill">
                        <i class="la la-search"></i> Filter Data
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>