<div class="m-portlet m-portlet--tab">
    <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
        <div class="m-portlet__body">
            <div class="form-group m-form__group row text-center">
                <label class="col-form-label col-lg-2 col-sm-12">Rentang Tanggal Data</label>
                <div class="col-lg- col-md-9 col-sm-12">
                    <input type='text'
                           class="form-control m_datepicker_1_modal"
                           readonly
                           value="{{ $date_from }}"
                           placeholder="Select time"
                           name="date_from"/>
                </div>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <input type='text'
                           class="form-control m_datepicker_1_modal"
                           readonly
                           value="{{ $date_to }}"
                           placeholder="Select time"
                           name="date_to"/>
                </div>
                <div class="col-lg-1 col-sm-12">
                    <button type="submit" class="btn btn-accent m-btn--pill">
                        <i class="la la-search"></i> Filter Data
                    </button>
                </div>
                <div class="col-lg-4 offset-lg-1 col-sm-12">
                    <a href="?date_from={{ date('d-m-Y') }}&date_to={{ date('d-m-Y') }}" class="btn btn-info m-btn--pill">
                        <i class="la la-search"></i> Data Hari Ini
                    </a>
                    <a href="?date_from={{ date('01-m-Y') }}&date_to={{ date('t-m-Y') }}"  class="btn btn-info m-btn--pill">
                        <i class="la la-search"></i> Data Bulan Ini
                    </a>
                    <a href="?date_from={{ date('d-m-Y', strtotime($date_first)) }}&date_to={{ date('t-m-Y') }}" class="btn btn-success m-btn--pill">
                        <i class="la la-search"></i> Semua Data
                    </a>
                </div>
            </div>
        </div>
    </form>
</div>