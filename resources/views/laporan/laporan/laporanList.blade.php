@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            {!! $date_filter !!}

            <div class="m-portlet m-portlet--tab">
                <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 col-sm-12">Rentang Tanggal Data</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value=""
                                       placeholder="Select time"
                                       name="date_from"/>
                            </div>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value=""
                                       placeholder="Select time"
                                       name="date_to"/>
                            </div>
                            <div class="col-lg-1 col-sm-12">
                                <button type="submit" class="btn btn-accent m-btn--pill">
                                    <i class="la la-search"></i> Filter Data
                                </button>
                            </div>
                            <div class="col-lg-4 offset-lg-1 col-sm-12">
                                <a href="?date_from={{ date('d-m-Y') }}&date_to={{ date('d-m-Y') }}" class="btn btn-info m-btn--pill">
                                    <i class="la la-search"></i> Data Hari Ini
                                </a>
                                <a href="?date_from={{ date('01-m-Y') }}&date_to={{ date('t-m-Y') }}"  class="btn btn-info m-btn--pill">
                                    <i class="la la-search"></i> Data Bulan Ini
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="m-portlet m-portlet--bordered-semi m-portlet--space m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                PROFIT :
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <button type="button"
                                        class="btn btn-success m-btn--pill cetak-pdf"
                                        data-url="{{ route('laporanCetakPdf') }}"
                                        data-data="{{ json_encode($table_data_post) }}">
                                    <i class="la la-print"></i> Cetak Laporan
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <div class="m-widget25">
                        @if($profit < 0)
                            <span class="m-widget25__price m--font-danger">
                                {{ \app\Helpers\Main::format_money($profit) }}
                            </span>
                        @else
                            <span class="m-widget25__price m--font-accent">
                                {{ \app\Helpers\Main::format_money($profit) }}
                            </span>
                        @endif
                        <span class="m-widget25__desc">
                            Total Profit sesuai dengan filter tanggal |
                            <small><i>formula : (penjualan - pembelian)</i></small>
                        </span>
                        <div class="m-widget25--progress">
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($pembelian_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    PEMBELIAN
                                </span>
                            </div>
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($penjualan_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-warning" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    PENJUALAN (OMSET)
                                </span>
                            </div>
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($hutang_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-accent" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    HUTANG
                                </span>
                            </div>
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($piutang_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-info" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    PIUTANG
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">

                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-pembelian" aria-expanded="true">
                        <i class="flaticon-shopping-basket"></i>
                        Pembelian
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-penjualan" aria-expanded="true">
                        <i class="flaticon-bell"></i>
                        Penjualan
                    </a>
                </li>
            </ul>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-pembelian" role="tabpanel" aria-expanded="true">
                        <div class="m-portlet__body">
                            <table class="table table-bordered datatable-new"
                                   data-url="{{ route('laporanPembelianDataTable') }}"
                                   data-column="{{ json_encode($datatable_column_pembelian) }}"
                                   data-data="{{ json_encode($table_data_post) }}">
                                <thead>
                                <tr>
                                    <th width="20">No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Barang</th>
                                    <th>Kode Batch</th>
                                    <th>Harga Beli</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-penjualan" role="tabpanel" aria-expanded="true">
                        <div class="m-portlet__body">
                            <table class="table table-bordered datatable-new"
                                   data-url="{{ route('laporanPenjualanDataTable') }}"
                                   data-column="{{ json_encode($datatable_column_penjualan) }}"
                                   data-data="{{ json_encode($table_data_post) }}">
                                <thead>
                                <tr>
                                    <th width="20">No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Barang</th>
                                    <th>Kode Batch</th>
                                    <th>Harga Jual</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
@endsection
